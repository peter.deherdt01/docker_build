FROM debian:buster

RUN apt-get update && \
    apt-get -y --no-install-recommends install \
    alien \
    bash-completion \
    ca-certificates \
    clang-tools \
    curl \
    dirmngr \
    doxygen \
    dumb-init \
    gcc \
    gcovr \
    git \
    gnupg \
    fakeroot \
    libcmocka-dev \
    make \
    pkg-config \
    sudo \ 
    uncrustify \
    valgrind && \
    echo "dash dash/sh boolean false" | debconf-set-selections && \
    DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash && \
    curl -s https://packagecloud.io/install/repositories/peter_deherdt/ambiorix/script.deb.sh | sudo bash && \
    apt-get -y remove \
    dirmngr \
    gnupg && \
    apt-get -q -y autoremove && \
    rm -rf /var/lib/apt/lists/* /var/tmp/* /tmp/*

ENV TERM xterm-256color
ENV LANG=C.UTF-8
COPY resources/ /
RUN chmod 644 /etc/sudoers

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/usr/local/bin/init.sh"]
